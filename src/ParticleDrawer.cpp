/**
 * @file ParticleDrawer.cpp
 * @author jmmut
 * @date 2017-08-23.
 */

#include "ParticleDrawer.h"
#include "SphereLOD.h"

void ParticleDrawer::draw(const Particle &particle) {
    SphereLOD sphere{particle.position, particle.radius, colorSeed++};
    sphere.draw(3);
}
