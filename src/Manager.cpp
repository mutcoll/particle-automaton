/**
 * @file Manager.cpp
 * @author jmmut
 * @date 2017-08-23.
 */

#include "Manager.h"
#include "ParticleDrawer.h"
#include "Physics.h"

using Particles = std::vector<Particle>;

Manager::Manager(Dispatcher &d, shared_ptr<WindowGL> w) : FlyerManagerBase( d, w){
    resetParticles();
}

void Manager::resetParticles() {
    particles.clear();
    float sin45 = 0.707107;    // this makes a bounce of 90 degrees
    particles.push_back({Vector3D{-5, 1.8, 2.1}, Vector3D{2, 0, 0}, 1, 20});
    for (size_t i = 0; i < 5; ++i) {
        for (size_t j = 0; j < 4; ++j) {
            for (size_t k = 0; k < 3; ++k) {
                particles.push_back({Vector3D{k, j, i}, Vector3D{}, 0.4, 1});
            }
        }
    }
    particles.push_back({Vector3D{0, -10000, 0}, Vector3D{0, 0, 0}, 10000, 2000});
}

void Manager::onDisplay(Uint32 ms) {
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glPushMatrix();

    this->FlyerManagerBase::onDisplay( ms);
    drawAxis();
    drawParticles();

    logMomentumAndEnergy();

    glPopMatrix();

    static_pointer_cast< WindowGL>( this->window.lock())->swapWindow();
}

void Manager::logMomentumAndEnergy() const {
    Vector3D accumulatedMomentum;
    for (const auto &particle : particles) {
        accumulatedMomentum += particle.speed * particle.mass;
    }
    float accumulatedEnergy;
    for (const auto &particle : particles) {
        accumulatedEnergy += 0.5 * particle.mass * particle.speed * particle.speed;
    }
//    cout << "momentum: " << accumulatedMomentum << "; energy: " << accumulatedEnergy << "\r";
}

void Manager::drawAxis() const {
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(100, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 100, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 100);
    glEnd();
}

void Manager::drawParticles() const {
    ParticleDrawer drawer;
    for (const auto &particle : particles) {
        drawer.draw(particle);
    }
}

void Manager::onStep(Uint32 ms) {
    MediumManagerBase::onStep(ms);
    iterateParticles(ms);
    interactParticles();
}

void Manager::iterateParticles(Uint32 ms) {
    Physics physics;
    for (auto &particle : particles) {
        physics.iterate(particle, ms);
    }
}

void Manager::interactParticles() {
    Physics physics;
    Particles newParticles;
    for (size_t i = 0; i < particles.size(); ++i) {
        for (size_t j = i + 1; j < particles.size(); ++j) {
//            Particles resultingParticles = physics.interaction(particles[i], particles[j]);
            physics.interaction(particles[i], particles[j]);
//            newParticles.insert(newParticles.end(),
//                    resultingParticles.begin(),
//                    resultingParticles.end());
        }
    }

//    swap(particles, newParticles);
}

void Manager::onKeyPressed(SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode) {
    FlyerManagerBase::onKeyPressed(keyCode, keyMod, scanCode);
    if (keyCode == SDLK_r) {
        resetParticles();
    }
}

