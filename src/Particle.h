/**
 * @file Particle.h
 * @author jmmut
 * @date 2017-08-23.
 */

#ifndef PARTICLEAUTOMATON_PARTICLE_H
#define PARTICLEAUTOMATON_PARTICLE_H


#include <math/Vector3D.h>

class Particle {
public:
    Vector3D position;
    Vector3D speed;
    double radius;
    double mass;
};


#endif //PARTICLEAUTOMATON_PARTICLE_H
