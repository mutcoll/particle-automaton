
# Particle automaton

Check that you have the requirements installed, and do the following:

Linux:

```
git clone https://bitbucket.org/mutcoll/particle-automaton.git
cd particle-automaton
conan install --build missing
mkdir build && cd build
cmake -G "Unix Makefiles" ..
make
./bin/parton
```

## Requirements

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended: `sudo apt-get install python-pip; sudo pip install conan`)

## To do

- particle
- collision
- particle generator
- particle configuration
- evolution
  - fitness
  - load bodies


### angel
- define properties
- define interactions
