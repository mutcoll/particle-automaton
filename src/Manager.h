/**
 * @file Manager.h
 * @author jmmut
 * @date 2017-08-23.
 */

#ifndef PARTICLEAUTOMATON_MANAGER_H
#define PARTICLEAUTOMATON_MANAGER_H


#include "manager_templates/FlyerManagerBase.h"
#include "Particle.h"

class Manager : public FlyerManagerBase {

public:
    Manager( Dispatcher &d, shared_ptr< WindowGL> w);

protected:
    virtual void onDisplay( Uint32 ms);

    virtual void onStep(Uint32 ms) override;

    virtual void onKeyPressed(SDL_Keycode keyCode, SDL_Keymod keyMod,
            SDL_Scancode scanCode) override;

private:
    std::vector<Particle> particles;

    void drawAxis() const;

    void drawParticles() const;

    void iterateParticles(Uint32 ms);

    void interactParticles();

    void resetParticles();

    void logMomentumAndEnergy() const;
};


#endif //PARTICLEAUTOMATON_MANAGER_H
