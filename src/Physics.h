/**
 * @file Physics.h
 * @author jmmut
 * @date 2017-08-23.
 */

#ifndef PARTICLEAUTOMATON_PHYSICS_H
#define PARTICLEAUTOMATON_PHYSICS_H


#include "Particle.h"
#include "manager_templates/FlyerManagerBase.h"

class Physics {

public:

    void iterate(Particle &particle, unsigned int ms);

    std::vector<Particle> interaction(Particle &first, Particle &second);

    bool spheresIntersect(Vector3D firstPosition, double firstRadius,
            Vector3D secondPosition, double secondRadius);

    vector<Particle> bouncingParticles(Particle &first, Particle &second);

    void elasticCollision(const Vector3D &firstSpeed, const Vector3D &secondSpeed,
            double firstMass, double secondMass,
            Vector3D &newFirstSpeed, Vector3D &newSecondSpeed) const;
};


#endif //PARTICLEAUTOMATON_PHYSICS_H
