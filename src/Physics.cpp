/**
 * @file Physics.cpp
 * @author jmmut
 * @date 2017-08-23.
 */

#include "Manager.h"
#include "Physics.h"

void Physics::iterate(Particle &particle, unsigned int ms) {
    particle.position += particle.speed * 0.001 * ms;
}

std::vector<Particle> Physics::interaction(Particle &first, Particle &second) {
    std::vector<Particle> newParticles;
    if (spheresIntersect(first.position, first.radius, second.position, second.radius)) {
        return bouncingParticles(first, second);
    }
    return {first, second};
}

bool Physics::spheresIntersect(Vector3D firstPosition, double firstRadius,
        Vector3D secondPosition, double secondRadius) {
    bool intersect = (firstPosition - secondPosition).Modulo() < firstRadius + secondRadius;
    return intersect;
}

std::vector<Particle> Physics::bouncingParticles(Particle &first, Particle &second) {
    Vector3D distance = second.position - first.position;
    Vector3D normal = distance.Unit(); // TODO check for 0

    float firstSpeedMagnitudeOverNormal = first.speed * normal;
    float secondSpeedMagnitudeOverNormal = second.speed * normal;
    if (firstSpeedMagnitudeOverNormal < 0 && secondSpeedMagnitudeOverNormal > 0) {
        // the particles are moving away from each other, let them do that without bouncing inwards
        return {first, second};
    }
    Vector3D firstNormal = firstSpeedMagnitudeOverNormal * normal;
    Vector3D secondNormal = secondSpeedMagnitudeOverNormal * normal;

    Vector3D newFirstSpeed;
    Vector3D newSecondSpeed;
    elasticCollision(
            firstNormal, secondNormal,
            first.mass, second.mass,
            newFirstSpeed, newSecondSpeed);

    newFirstSpeed += first.speed - firstNormal;
    newSecondSpeed += second.speed - secondNormal;

    Particle newFirst{first.position, newFirstSpeed, first.radius, first.mass};
    Particle newSecond{second.position, newSecondSpeed, second.radius, second.mass};
    std::swap(first, newFirst);
    std::swap(second, newSecond);

    return {newFirst, newSecond};
}

/**
 * https://en.wikipedia.org/wiki/Elastic_collision#One-dimensional_Newtonian
 */
void Physics::elasticCollision(const Vector3D &firstSpeed, const Vector3D &secondSpeed,
        double firstMass, double secondMass,
        Vector3D &newFirstSpeed, Vector3D &newSecondSpeed) const {

    double masses = firstMass + secondMass;
    Vector3D firstMomentum = firstMass * firstSpeed;
    Vector3D secondMomentum = secondMass * secondSpeed;

    newFirstSpeed = (firstSpeed * (firstMass - secondMass) + 2 * secondMomentum) / masses;
    newSecondSpeed = (secondSpeed * (secondMass - firstMass) + 2 * firstMomentum) / masses;
}
