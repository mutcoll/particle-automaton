/**
 * @file SphereLOD.h
 * @author jmmut 
 * @date 2016-04-13.
 */

#ifndef GRAVITYTAMER_SPHERELOD_H
#define GRAVITYTAMER_SPHERELOD_H

#include <algorithm>
#include <array>
#include <random>
#include "math/Vector3D.h"

class SphereLOD {
public:

    SphereLOD(const Vector3D &center, double radius)
            : center(center), radius(radius), distribution(0.5, 1.0) {
    }
    SphereLOD(const Vector3D &center, double radius, long colorSeed)
            : center(center), radius(radius), generator(colorSeed), distribution(0.5, 1.0),
            colorSeed(colorSeed) {
    }

    void draw(double polygonOrderOfMagnitude);
    Vector3D & getPos();
    double & getRadius();
    int getFacesDrawn();

private:
    Vector3D center;
    double radius;
    std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution;
    // distance in a cube from center to an edge if the distance to a vertex is 1
    float apothem;
    int facesDrawn;
    long colorSeed;

    void drawFace(int detail, std::array<Vector3D, 3> faceVertices);

    Vector3D middleVertex(Vector3D u, Vector3D v);

    float hash(int seed) const;
};


#endif //GRAVITYTAMER_SPHERELOD_H
