/**
 * @file ParticleDrawer.h
 * @author jmmut
 * @date 2017-08-23.
 */

#ifndef PARTICLEAUTOMATON_PARTICLEDRAWER_H
#define PARTICLEAUTOMATON_PARTICLEDRAWER_H

#include "Particle.h"

class ParticleDrawer {
public:
    void draw(const Particle &particle);
    int colorSeed = 1;
};


#endif //PARTICLEAUTOMATON_PARTICLEDRAWER_H
